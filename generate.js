const {Elm} = require("./lib/elm")
const generator = Elm.Main.init()

generator.ports.codeOut.subscribe((code) => {
  console.log(code)
})

generator.ports.schemaIn.send(null)
