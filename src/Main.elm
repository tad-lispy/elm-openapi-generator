port module Main exposing (main)

import Elm.Syntax.Comments as Comments exposing (Comment)
import Elm.Syntax.Declaration as Declaration exposing (Declaration(..))
import Elm.Syntax.Exposing as Exposing
import Elm.Syntax.Expression as Expression
    exposing
        ( Expression(..)
        , Function
        )
import Elm.Syntax.File as File exposing (File)
import Elm.Syntax.Module as Module exposing (Module(..))
import Elm.Syntax.Node as Node exposing (Node(..))
import Elm.Syntax.Range as Range exposing (Range)
import Elm.Writer
import Json.Decode exposing (Decoder)
import Json.Encode exposing (Value)


port schemaIn : (Value -> msg) -> Sub msg


port codeOut : Value -> Cmd msg


port errorsOut : Value -> Cmd msg


main : Program Flags Model Msg
main =
    Platform.worker
        { init = init
        , update = update
        , subscriptions = subscriptions
        }


type alias Flags =
    ()


type alias Model =
    ()


type Msg
    = SchemaReceived (Result Json.Decode.Error Schema)


init : Flags -> ( Model, Cmd Msg )
init flags =
    ( ()
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SchemaReceived (Ok schema) ->
            ( model
            , schema
                |> generateElm
                |> Elm.Writer.writeFile
                |> Elm.Writer.write
                |> Json.Encode.string
                |> codeOut
            )

        SchemaReceived (Err error) ->
            ( model
            , error
                |> Json.Decode.errorToString
                |> Json.Encode.string
                |> errorsOut
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    schemaIn (decodeSchema SchemaReceived)


decodeSchema :
    (Result Json.Decode.Error Schema -> Msg)
    -> Value
    -> Msg
decodeSchema constructor json =
    json
        |> Json.Decode.decodeValue schemaDecoder
        |> constructor


schemaDecoder : Decoder Schema
schemaDecoder =
    Json.Decode.succeed Placeholder


type Schema
    = Placeholder


generateElm : Schema -> File
generateElm schema =
    case schema of
        Placeholder ->
            { moduleDefinition =
                syntheticNode <|
                    NormalModule
                        { moduleName =
                            syntheticNode [ "Main" ]
                        , exposingList =
                            syntheticNode
                                (Exposing.Explicit
                                    [ syntheticNode <| Exposing.FunctionExpose "main" ]
                                )
                        }
            , imports =
                [ syntheticNode
                    { moduleName = syntheticNode [ "Html" ]
                    , moduleAlias = Nothing
                    , exposingList =
                        Just <|
                            syntheticNode <|
                                Exposing.Explicit
                                    [ syntheticNode <| Exposing.TypeOrAliasExpose "Html"
                                    ]
                    }
                ]
            , declarations =
                [ syntheticNode <|
                    FunctionDeclaration
                        { documentation = Nothing
                        , signature = Nothing
                        , declaration =
                            syntheticNode
                                { name = syntheticNode "main"
                                , arguments = []
                                , expression =
                                    syntheticNode <|
                                        Application
                                            [ syntheticNode <| FunctionOrValue [ "Html" ] "text"
                                            , syntheticNode <| Literal "Hello, I'm generated!"
                                            ]
                                }
                        }
                ]
            , comments = [ syntheticNode "This is a generated code" ]
            }


syntheticNode : a -> Node a
syntheticNode data =
    Node Range.emptyRange data
