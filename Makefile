.PHONY: all
all: dist

.PHONY: dist
dist: lib/elm.js generate.js

lib/elm.js: src/Main.elm
	elm make --output=lib/elm.js --optimize src/Main.elm

.PHONY: run
run: dist
	node generate.js \
		| elm-format --stdin
