# Generating Elm code from OpenAPI document

Scope question: we can generate entire HTTP client or just JSON decoders and encoders.

## Idea

The OpenAPI document has to conform to JSON spec (even if it's encoded in YAML it cannot use language features incompatible with JSON). So we can implement a decoder that will get us a custom data structure representing entire document. Then we can transform it into an [Elm Syntax File](https://package.elm-lang.org/packages/stil4m/elm-syntax/latest/Elm-Syntax-File) and then write this file to disk.
